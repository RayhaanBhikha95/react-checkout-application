const path = require('path');
const webpack = require('webpack');
const baseConfig = require('./webpack-base.config.js');
const merge = require('webpack-merge');


const devConfig = {
    devtool: 'inline-source-map',
        devServer: {
            contentBase: path.resolve('dist'),
            hot: true,
            port: 8000
        },
        plugins: [
            new webpack.NamedModulesPlugin(),
            new webpack.HotModuleReplacementPlugin()
    ]
}

module.exports = merge(baseConfig, devConfig);