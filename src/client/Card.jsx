import React from 'react'
// import ReactDOM from 'react-dom'
import './style.scss'

export default class Items extends React.Component {
  render () {
    var { item, addToBasket } = this.props
    return (
      <div className='card cardStyle col-md-8 offset-md-2 pt-2'>
        <div className='bg-primary text-white h2 p-2 pl-3'>{item.id}</div>
        <div className='card-body p-3'>
          <span className='h2 priceStyle'>{item.price}</span>
          <h5 className='card-title'>{item.name}</h5>
          <h6 className='card-subtitle mb-2 text-muted'>{item.type}</h6>
          <p className='card-text'>{item.details}</p>
          <span>
            <strong>
              Specifications:
            </strong>
          </span>
          <ul>
            {
              Object.keys(item.specs).map((field, index) => <li key={index}>{item.specs[field]}</li>)
            }
          </ul>
          <button className='btn btn-warning buttonStyle addToBasketBtn' index={this.props.index} product={JSON.stringify(item)}
            onClick={(e) => {
              e.target.setAttribute('disabled', true)
              addToBasket(e)
            }
            }>Add To Basket</button>
        </div>
      </div>
    )
  }
}
