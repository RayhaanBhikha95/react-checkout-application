const express = require('express')
const app = express.Router()

var data = [
  {
    id: '1',
    type: 'Laptop',
    name: 'Asus ux305FA',
    category: 'appliances',
    price: '£700',
    vat: '10%',
    details: 'Asus laptop 2015, very thin.',
    specs: {
      'CPU': '900 MHz Intel Core M3',
      'Graphics': 'Intel HD Graphics 515.',
      'RAM': '8 GB DDR3L(1, 866 MHz SDRAM)',
      'Screen': '13.3 - inch.'
    }
  },
  {
    id: '2',
    type: 'Mobile',
    name: 'Samsung Galaxy S6',
    category: 'appliances',
    price: '£300',
    vat: '15%',
    details: 'Brand new Samsung s6 phone, with 20mp camera',
    specs: {
      'Dimension': '.70.5 x 143.4 x 6.8 mm(138 g)',
      'Camera.Rear': '16 MP OIS(F1 .9) Front: 5 MP(F1 .9)',
      'Display': '.5.1” Quad HD Super AMOLED.2560 x 1440(577 ppi)',
      'AP': '.Exynos 7420(64 - bit, 14 nm), Octa core(2.1 GHz Quad + 1.5 GHz Quad)',
      'OS': '.Android 5.0(Lollipop)',
      'Network': '.LTE Cat 6',
      'Memory': '3 GB RAM(LPDDR4)...attery.2550 mAh.'
    }
  }
]

app.get('/products', (req, res) => {
  console.log('server was hit')
  res.send(data)
})

module.exports = app
