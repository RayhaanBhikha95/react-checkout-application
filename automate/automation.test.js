import { toMatchImageSnapshot } from 'jest-image-snapshot'
import puppeteer from 'puppeteer'

const appUrl = 'http://localhost:8001'
// const fileName = new Date().getTime();

describe('Should be able to checkout product', () => {
  let browser
  let page

  beforeEach(async () => {
    try {
      expect.extend({ toMatchImageSnapshot })
      browser = await puppeteer.launch()

      page = await browser.newPage()
    } catch (e) {
      console.log(e)
    }
  })

  afterAll(() => {
    browser.close()
  })

  test('Should be able to checkout a product', async () => {
    await page.goto(appUrl)
    const snapshot = await page.screenshot()
    expect(snapshot).toMatchImageSnapshot()
  })
})

// (async () => {
//   try {
//     const browser = await puppeteer.launch(
//       {
//         headless: false
//       }
//     )
//     const page = await browser.newPage()

//     await page.goto(appUrl) // navigate page to url
//     const snapshot = await page.screenshot() // take screenshot of page.
//     expect(snapshot).toMatchImageSnapshot()

//     // // enter products page.
//     await page.click('#productPageBtn', {
//       delay: 2000
//     })

//     // // add items to basket.
//     // const buttons = await page.select('.addToBasketBtn', 'btn', 'btn-warning', 'buttonStyle');

//     // item 1.
//     await page.waitForSelector('.addToBasketBtn')
//     await page.click('.addToBasketBtn', {
//       delay: 2000
//     })

//     // checkout.
//     await page.waitForSelector('#checkoutBtn')
//     await page.click('#checkoutBtn', {
//       delay: 2000
//     })
//     // checkoutBtn

//     // Alert
//     // closeAlertBtn

//     // homePage
//     // homePageBtn

//     await page.waitFor(5000)
//     await browser.close()
//   } catch (err) {
//     console.log('THERE WAS AN ERROR: ', err)
//   }
// })
